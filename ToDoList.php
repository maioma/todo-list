<?php
class Task {
    private $taskName;
    private $taskDescription;
    private $taskDueDate;
    private $isCompleted;

    public function __construct($taskName, $taskDescription, $taskDueDate) {
        $this->taskName = $taskName;
        $this->taskDescription = $taskDescription;
        $this->taskDueDate = $taskDueDate;
        $this->isCompleted = false;
    }

    public function completeTask() {
        $this->isCompleted = true;
    }

    public function getTaskName() {
        return $this->taskName;
    }

    public function getTaskDescription() {
        return $this->taskDescription;
    }

    public function getTaskDueDate() {
        return $this->taskDueDate;
    }

    public function isCompleted() {
        return $this->isCompleted;
    }
}


class ToDoList {
    private $tasks = [];

    public function addTask($task) {
        $this->tasks[] = $task;
    }

    public function removeTask($index) {
        if (isset($this->tasks[$index])) {
            array_splice($this->tasks, $index, 1);
        }
    }

    public function getTasks() {
        return $this->tasks;
    }
}

// Create a ToDoList instance
$toDoList = new ToDoList();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $taskName = $_POST['taskName'];
    $taskDescription = $_POST['taskDescription'];
    $taskDueDate = $_POST['taskDueDate'];

    $newTask = new Task($taskName, $taskDescription, $taskDueDate);
    $toDoList->addTask($newTask);
}

// Display the tasks
$tasks = $toDoList->getTasks();
?>

<!DOCTYPE html>
<html>
<head>
    <title>ToDo List</title>
</head>
<style>
    body {
        background-color: #17181D;
        color: #FCD9B8;
        font-family: Arial, sans-serif;
        margin: 0;
        padding: 0;
    }

    h1 {
        background-color: #292C35;
        color: #E09145;
        padding: 20px;
        text-align: center;
    }

    form {
        margin: 20px;
    }

    input[type="text"],
    input[type="date"] {
        width: 100%;
        padding: 10px;
        margin: 5px 0;
        background-color: #292C35;
        color: #FCD9B8;
        border: none;
    }

    button {
        background-color: #E09145;
        color: #17181D;
        padding: 10px 20px;
        border: none;
        cursor: pointer;
    }

    button:hover {
        background-color: #FCD9B8;
    }

    ul {
        list-style-type: none;
        padding: 0;
    }

    li {
        background-color: #292C35;
        margin: 5px;
        padding: 10px;
        border: 1px solid #E09145;
        display: flex;
        justify-content: space-between;
    }
</style>


<body>
    <h1>ToDo List</h1>
    <form method="post">
        <input type="text" name="taskName" placeholder="Task Name">
        <input type="text" name="taskDescription" placeholder="Task Description">
        <input type="date" name="taskDueDate">
        <button type="submit">Add Task</button>
    </form>

    <ul>
        <?php foreach ($tasks as $index => $task) { ?>
            <li>
                <?php echo $task->getTaskName(); ?> - <?php echo $task->getTaskDueDate(); ?>
            </li>
        <?php } ?>
    </ul>
</body>
</html>
